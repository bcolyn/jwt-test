package test.jwt;

import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.KeyPair;
import java.security.PublicKey;

public class Main {
    public static void main(String[] args) throws Exception {

        KeyPair kp = readPrivateKey("/jwt-private.pem");

        String compactJws = Jwts.builder()
                .setSubject("Benny")
                .signWith(SignatureAlgorithm.RS256, kp.getPrivate())
                .compact();

        System.out.println(compactJws);

        PublicKey jcaPubKey = readPublicKey("/jwt-public.pem");

        Jwt jwt = Jwts.parser()
                .setSigningKey(jcaPubKey)
                .parse(compactJws);

        System.out.println(jwt);
    }

    private static KeyPair readPrivateKey(String resName) throws IOException {
        try (InputStream ins = Main.class.getResourceAsStream(resName); Reader rdr = new InputStreamReader(ins)){
            PEMParser parser = new PEMParser(rdr);
            PEMKeyPair pemKeyPair = (PEMKeyPair) parser.readObject();
            return new JcaPEMKeyConverter().getKeyPair(pemKeyPair);
        }
    }

    private static PublicKey readPublicKey(String resName) throws IOException {
        try (InputStream ins = Main.class.getResourceAsStream(resName); Reader rdr = new InputStreamReader(ins)) {
            PEMParser pubparser = new PEMParser(rdr);
            SubjectPublicKeyInfo pubKey = (SubjectPublicKeyInfo) pubparser.readObject();
            return new JcaPEMKeyConverter().getPublicKey(pubKey);
        }
    }
}
